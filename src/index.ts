import express, { json } from 'express';
import morgan from 'morgan';
import path from 'path';
import fs from 'fs';
import {connection, handleDisconnect} from './dbconnect';

import bodyparser from 'body-parser';
import cors from 'cors';
import { Restaurant } from './restaurant';
import { Menu } from './menu';
import { Order } from './order';
import { RestaurantRating } from './restaurant-rating';

const port:number = 3000;

let app = express();

app.use(cors());
app.use(bodyparser.urlencoded({extended: false}));
app.use(bodyparser.json());
app.use(morgan('combined',{
    stream: fs.createWriteStream(path.join(__dirname, '../access.log'), {flags: 'a'})
}));

app.get('/', (req, res)=>{
    res.send("This is the index page");
});

app.get('/restaurant',  (req, res)=>{
    console.log("inside the get '/restaurant' route");
    connection.query('SELECT * FROM restaurant', (err, result)=>{
        if(err){
            console.log("query error: " + err);
            res.json({"Error": err});
        }
        else{
            res.json(result);
        }
    })
});



app.get('/restaurant/:id', (req, res)=> {

    let id = req.params['id'];
    connection.query('SELECT * FROM restaurant WHERE id=?', id, (err, result)=>{
        if(err){
            console.log("Error: " + err);
            res.json({'Error': err});
        }
        else{
            res.json(result);
        }
    });
});

app.get('/menu',  (req, res)=>{
    console.log("inside the get '/menu' route");
    connection.query('SELECT * FROM menu', (err, result)=>{
        if(err){
            console.log("query error: " + err);
            res.json({"Error": err});
        }
        else{
            res.json(result);
        }
    })
});

app.get('/menu/:id', (req, res)=> {

    let id = req.params['id'];
    connection.query('SELECT * FROM menu WHERE rest_id=?', id, (err, result)=>{
        if(err){
            console.log("Error: " + err);
            res.json({'Error': err});
        }
        else{
            res.json(result);
        }
    });
});
app.get('/menu-order/:id', (req, res)=> {

    let id = req.params['id'];
    connection.query('SELECT * FROM menu WHERE id=?', id, (err, result)=>{
        if(err){
            console.log("Error: " + err);
            res.json({'Error': err});
        }
        else{
            res.json(result);
        }
    });
});

app.get('/orders',  (req, res)=>{
    console.log("inside the get '/orders' route");
    connection.query('SELECT * FROM orders', (err, result)=>{
        if(err){
            console.log("query error: " + err);
            res.json({"Error": err});
        }
        else{
            res.json(result);
        }
    })
});

app.get('/orders/:id', (req, res)=> {

    let id = req.params['id'];
    connection.query('SELECT * FROM orders WHERE customer_id=?', id, (err, result)=>{
        if(err){
            console.log("Error: " + err);
            res.json({'Error': err});
        }
        else{
            res.json(result);
        }
    });
});

app.get('/rating',  (req, res)=>{
    console.log("inside the get '/rating' route");
    connection.query('SELECT * FROM rating', (err, result)=>{
        if(err){
            console.log("query error: " + err);
            res.json({"Error": err});
        }
        else{
            res.json(result);
        }
    })
});

app.get('/rating/:id', (req, res)=> {

    let id = req.params['id'];
    connection.query('SELECT AVG(rating) AS rate FROM rating WHERE rest_id=?', id, (err, result:number)=>{
        if(err){
            console.log("Error: " + err);
            res.json({'Error': err});
        }
        else{
            res.json(result);
        }
    });
});



app.post('/restaurant', (req, res)=>{

    let restaurant: Restaurant = req.body.restaurant;
    connection.query(`
    INSERT INTO restaurant (name, city, lat, lng, phone, image) VALUES
    ('${restaurant.name}', '${restaurant.city}', ${restaurant.lat},${restaurant.lng},${restaurant.phone},'${restaurant.image}');
    `, (err, result)=>{
        if(err){
            console.log('Error '+ err);
            res.json({'Error': err});
        }
        else{
            res.json({'Created': result});
        }
    });
});

app.post('/menu', (req, res)=>{

    let menu: Menu = req.body.menu;
    connection.query(`
    INSERT INTO menu (name, descr, price, rest_id, image) VALUES
    ('${menu.name}', '${menu.descr}', ${menu.price},${menu.rest_id},'${menu.image}');
    `, (err, result)=>{
        if(err){
            console.log('Error '+ err);
            res.json({'Error': err});
        }
        else{
            res.json({'Created': result});
        }
    });
});

app.post('/orders', (req, res)=>{

    let order: Order = req.body.orders;
    connection.query(`
    INSERT INTO orders (rest_id, menu_id, customer_id, quantity) VALUES
    (${order.rest_id}, ${order.menu_id}, ${order.customer_id},${order.quantity});
    `, (err, result)=>{
        if(err){
            console.log('Error '+ err);
            res.json({'Error': err});
        }
        else{
            res.json({'Created': result});
        }
    });
});

app.post('/rating', (req, res)=>{

    let rating: RestaurantRating = req.body.rating;
    connection.query(`
    INSERT INTO rating (rest_id, rating, customer_id) VALUES
    (${rating.rest_id}, ${rating.rating}, ${rating.customer_id});
    `, (err, result)=>{
        if(err){
            console.log('Error '+ err);
            res.json({'Error': err});
        }
        else{
            res.json({'Created': result});
        }
    });
});



app.put('/restaurant/:id', (req, res)=>{
    let id = req.params['id'];
    let restaurant: Restaurant = req.body.restaurant;

    connection.query(`UPDATE restaurant SET name='${restaurant.name}', city='${restaurant.city}', lat= ${restaurant.lat} ,lng =${restaurant.lng} ,phone=${restaurant.phone} ,image='${restaurant.image}' WHERE id=${id}`, (err, result)=>{
        if(err){
            res.status(404).json({"Error": err});
            console.log(err);
        }
        else{
            res.json({'Success': result});
        }
    })
});
app.put('/menu/:id', (req, res)=>{
    let id = req.params['id'];
    let menu: Menu = req.body.menu;

    connection.query(`UPDATE menu SET name='${menu.name}', descr='${menu.descr}' ,price=${menu.price}, rest_id=${menu.rest_id} ,image='${menu.image}' WHERE id=${id}`, (err, result)=>{
        if(err){
            res.status(404).json({"Error": err});
            console.log(err);
        }
        else{
            res.json({'Success': result});
        }
    })
});

app.delete('/restaurant/:id', (req, res)=>{
    let id = req.params['id'];
    connection.query("DELETE FROM restaurant WHERE id=?", id, (err, result)=>{
        if(err){
            console.log(err);
            res.status(404).json({"Error": err});
        }
        else{
            res.json({"success": result});
        }
    })
});

app.delete('/orders/:id', (req, res)=>{
    let id = req.params['id'];
    connection.query("DELETE FROM orders WHERE id=?", id, (err, result)=>{
        if(err){
            console.log(err);
            res.status(404).json({"Error": err});
        }
        else{
            res.json({"success": result});
        }
    })
});






app.listen(port, ()=>{
    console.log("Server is listening on port " + port);
});

