"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Restaurant = void 0;
class Restaurant {
    constructor(name, city, phone, image, rating, id, lat, lng) {
        this.name = name;
        this.city = city;
        this.phone = phone;
        this.image = image;
        this.rating = rating;
        this.id = id;
        this.lat = lat;
        this.lng = lng;
    }
}
exports.Restaurant = Restaurant;
//# sourceMappingURL=restaurant.js.map