"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Order = void 0;
class Order {
    constructor(rest_id, menu_id, customer_id, quantity, date_created, id) {
        this.rest_id = rest_id;
        this.menu_id = menu_id;
        this.customer_id = customer_id;
        this.quantity = quantity;
        this.date_created = date_created;
        this.id = id;
    }
}
exports.Order = Order;
//# sourceMappingURL=order.js.map