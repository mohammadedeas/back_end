"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestaurantRating = void 0;
class RestaurantRating {
    constructor(customer_id, rest_id, rating, id, date_rated) {
        this.customer_id = customer_id;
        this.rest_id = rest_id;
        this.rating = rating;
        this.id = id;
        this.date_rated = date_rated;
    }
}
exports.RestaurantRating = RestaurantRating;
//# sourceMappingURL=restaurant-rating.js.map