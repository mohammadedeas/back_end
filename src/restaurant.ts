export class Restaurant {
    constructor(public name:string, public city:string, 
        public phone:string, public image:string, public rating:number,public id:number,public lat:number,public lng:number){

    }
}
