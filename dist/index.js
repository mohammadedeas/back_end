"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const dbconnect_1 = require("./dbconnect");
const body_parser_1 = __importDefault(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const port = 3000;
let app = (0, express_1.default)();
app.use((0, cors_1.default)());
app.use(body_parser_1.default.urlencoded({ extended: false }));
app.use(body_parser_1.default.json());
app.use((0, morgan_1.default)('combined', {
    stream: fs_1.default.createWriteStream(path_1.default.join(__dirname, '../access.log'), { flags: 'a' })
}));
app.get('/', (req, res) => {
    res.send("This is the index page");
});
app.get('/restaurant', (req, res) => {
    console.log("inside the get '/restaurant' route");
    dbconnect_1.connection.query('SELECT * FROM restaurant', (err, result) => {
        if (err) {
            console.log("query error: " + err);
            res.json({ "Error": err });
        }
        else {
            res.json(result);
        }
    });
});
app.get('/restaurant/:id', (req, res) => {
    let id = req.params['id'];
    dbconnect_1.connection.query('SELECT * FROM restaurant WHERE id=?', id, (err, result) => {
        if (err) {
            console.log("Error: " + err);
            res.json({ 'Error': err });
        }
        else {
            res.json(result);
        }
    });
});
app.get('/menu', (req, res) => {
    console.log("inside the get '/menu' route");
    dbconnect_1.connection.query('SELECT * FROM menu', (err, result) => {
        if (err) {
            console.log("query error: " + err);
            res.json({ "Error": err });
        }
        else {
            res.json(result);
        }
    });
});
app.get('/menu/:id', (req, res) => {
    let id = req.params['id'];
    dbconnect_1.connection.query('SELECT * FROM menu WHERE rest_id=?', id, (err, result) => {
        if (err) {
            console.log("Error: " + err);
            res.json({ 'Error': err });
        }
        else {
            res.json(result);
        }
    });
});
app.get('/menu-order/:id', (req, res) => {
    let id = req.params['id'];
    dbconnect_1.connection.query('SELECT * FROM menu WHERE id=?', id, (err, result) => {
        if (err) {
            console.log("Error: " + err);
            res.json({ 'Error': err });
        }
        else {
            res.json(result);
        }
    });
});
app.get('/orders', (req, res) => {
    console.log("inside the get '/orders' route");
    dbconnect_1.connection.query('SELECT * FROM orders', (err, result) => {
        if (err) {
            console.log("query error: " + err);
            res.json({ "Error": err });
        }
        else {
            res.json(result);
        }
    });
});
app.get('/orders/:id', (req, res) => {
    let id = req.params['id'];
    dbconnect_1.connection.query('SELECT * FROM orders WHERE customer_id=?', id, (err, result) => {
        if (err) {
            console.log("Error: " + err);
            res.json({ 'Error': err });
        }
        else {
            res.json(result);
        }
    });
});
app.get('/rating', (req, res) => {
    console.log("inside the get '/rating' route");
    dbconnect_1.connection.query('SELECT * FROM rating', (err, result) => {
        if (err) {
            console.log("query error: " + err);
            res.json({ "Error": err });
        }
        else {
            res.json(result);
        }
    });
});
app.get('/rating/:id', (req, res) => {
    let id = req.params['id'];
    dbconnect_1.connection.query('SELECT AVG(rating) AS rate FROM rating WHERE rest_id=?', id, (err, result) => {
        if (err) {
            console.log("Error: " + err);
            res.json({ 'Error': err });
        }
        else {
            res.json(result);
        }
    });
});
app.post('/restaurant', (req, res) => {
    let restaurant = req.body.restaurant;
    dbconnect_1.connection.query(`
    INSERT INTO restaurant (name, city, lat, lng, phone, image) VALUES
    ('${restaurant.name}', '${restaurant.city}', ${restaurant.lat},${restaurant.lng},${restaurant.phone},'${restaurant.image}');
    `, (err, result) => {
        if (err) {
            console.log('Error ' + err);
            res.json({ 'Error': err });
        }
        else {
            res.json({ 'Created': result });
        }
    });
});
app.post('/menu', (req, res) => {
    let menu = req.body.menu;
    dbconnect_1.connection.query(`
    INSERT INTO menu (name, descr, price, rest_id, image) VALUES
    ('${menu.name}', '${menu.descr}', ${menu.price},${menu.rest_id},'${menu.image}');
    `, (err, result) => {
        if (err) {
            console.log('Error ' + err);
            res.json({ 'Error': err });
        }
        else {
            res.json({ 'Created': result });
        }
    });
});
app.post('/orders', (req, res) => {
    let order = req.body.orders;
    dbconnect_1.connection.query(`
    INSERT INTO orders (rest_id, menu_id, customer_id, quantity) VALUES
    (${order.rest_id}, ${order.menu_id}, ${order.customer_id},${order.quantity});
    `, (err, result) => {
        if (err) {
            console.log('Error ' + err);
            res.json({ 'Error': err });
        }
        else {
            res.json({ 'Created': result });
        }
    });
});
app.post('/rating', (req, res) => {
    let rating = req.body.rating;
    dbconnect_1.connection.query(`
    INSERT INTO rating (rest_id, rating, customer_id) VALUES
    (${rating.rest_id}, ${rating.rating}, ${rating.customer_id});
    `, (err, result) => {
        if (err) {
            console.log('Error ' + err);
            res.json({ 'Error': err });
        }
        else {
            res.json({ 'Created': result });
        }
    });
});
app.put('/restaurant/:id', (req, res) => {
    let id = req.params['id'];
    let restaurant = req.body.restaurant;
    dbconnect_1.connection.query(`UPDATE restaurant SET name='${restaurant.name}', city='${restaurant.city}', lat= ${restaurant.lat} ,lng =${restaurant.lng} ,phone=${restaurant.phone} ,image='${restaurant.image}' WHERE id=${id}`, (err, result) => {
        if (err) {
            res.status(404).json({ "Error": err });
            console.log(err);
        }
        else {
            res.json({ 'Success': result });
        }
    });
});
app.put('/menu/:id', (req, res) => {
    let id = req.params['id'];
    let menu = req.body.menu;
    dbconnect_1.connection.query(`UPDATE menu SET name='${menu.name}', descr='${menu.descr}' ,price=${menu.price}, rest_id=${menu.rest_id} ,image='${menu.image}' WHERE id=${id}`, (err, result) => {
        if (err) {
            res.status(404).json({ "Error": err });
            console.log(err);
        }
        else {
            res.json({ 'Success': result });
        }
    });
});
app.delete('/restaurant/:id', (req, res) => {
    let id = req.params['id'];
    dbconnect_1.connection.query("DELETE FROM restaurant WHERE id=?", id, (err, result) => {
        if (err) {
            console.log(err);
            res.status(404).json({ "Error": err });
        }
        else {
            res.json({ "success": result });
        }
    });
});
app.delete('/orders/:id', (req, res) => {
    let id = req.params['id'];
    dbconnect_1.connection.query("DELETE FROM orders WHERE id=?", id, (err, result) => {
        if (err) {
            console.log(err);
            res.status(404).json({ "Error": err });
        }
        else {
            res.json({ "success": result });
        }
    });
});
app.listen(port, () => {
    console.log("Server is listening on port " + port);
});
//# sourceMappingURL=index.js.map