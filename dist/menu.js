"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Menu = void 0;
class Menu {
    constructor(name, descr, price, image, rating, id, rest_id) {
        this.name = name;
        this.descr = descr;
        this.price = price;
        this.image = image;
        this.rating = rating;
        this.id = id;
        this.rest_id = rest_id;
    }
}
exports.Menu = Menu;
//# sourceMappingURL=menu.js.map